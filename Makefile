default: win

win: win.c
	gcc -Wall -o win win.c

clean:
	rm -f *~ *.o win
